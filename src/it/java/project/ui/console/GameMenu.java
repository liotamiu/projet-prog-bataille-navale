package it.java.project.ui.console;

import it.java.project.antotation.Test;
import it.java.project.entity.AShipEntity;
import it.java.project.entity.PlayerEntity;
import it.java.project.io.GameIOHandler;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * @author Miu
 */
public class GameMenu {

    private Grid grid;

    private Scanner sc;

    private GameIOHandler ioHandler;

    public void run() {
        init();
        loop();
    }

    private void init() {
        this.sc = new Scanner(System.in);
        this.ioHandler = new GameIOHandler();
        setSize(start());
    }

    private void loop() {
        ArrayList<AShipEntity> shipEntities1 = new ArrayList<>();

        Player_1 : while (true) {

            for (int i = 0; i < shipEntities1.size(); i++) {
                System.out.println("ID | NAME");
                System.out.println(i + " | " + shipEntities1.get(i).getName());
            }

            System.out.println();

            System.out.print("player1@pc $> ");
            String v = this.sc.nextLine();
            String[] vv = v.split(" ");

            switch (vv[0]) {
                case "set":
                    break Player_1;
                case "help":
                    System.out.println("help: show this.\n" +
                            "set: set the current list done.\n" +
                            "rm <id>: remove a ship.");
                    break;
                case "rm":
                    if (vv.length == 2) {
                        shipEntities1.remove(Integer.parseInt(vv[1]));
                    }
                    break;
            }
        }
    }

    private boolean start() {
        while (true) {
            System.out.print("start@pc $> ");
            String v = this.sc.next();
            switch (v) {
                case "play":
                    return true;
                case "help":
                    System.out.println("play: play a new game.\n" +
                            "help: show this.\n" +
                            "load: load a game save.");
                    break;
                case "load":
                    return false;
            }
        }
    }

    private void startSave() {
        System.out.print("save@pc $> filedir -d ");
        String path = this.sc.nextLine();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss");
        LocalDateTime now = LocalDateTime.now();
        File file = new File(path + "battleship_" + dtf.format(now) + ".save");
        try {
            this.ioHandler.saveGame(file, this.grid);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void startLoad() {
        System.out.print("load@pc $> file -f ");
        String path = this.sc.nextLine();
        File file = new File(path);
        try {
            this.grid = this.ioHandler.loadGame(file);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void setSize(boolean b) {
        if (!b) {
            System.out.println("loading system...");
            startLoad();
            return;
        }
        int height, width;

        System.out.println("╔═════════════════════════════════════════════════╗\n" +
                           "║                    Battleship                   ║\n" +
                           "╠═════════════════════════════════════════════════╣\n" +
                           "║                 Select Grid Size                ║\n" +
                           "╚═════════════════════════════════════════════════╝");

        do {
            try {
                System.out.print("size@pc $> height -v ");
                height = sc.nextInt();
                break;
            } catch (InputMismatchException e) {
                sc.nextLine(); // empty the buffer
            }
        } while (true);

        do {
            try {
                System.out.print("size@pc $> width -v ");
                width = sc.nextInt();
                break;
            } catch (InputMismatchException e) {
                sc.nextLine(); // empty the buffer
            }
        } while (true);

        String space = "                 ";
        space = space.substring(0, space.length() - ((String.valueOf(width).length() - 1) + (String.valueOf(height).length() - 1)));

        System.out.println("╔═════════════════════════════════════════════════╗\n" +
                           "║                    Battleship                   ║\n" +
                           "╠═════════════════════════════════════════════════╣\n" +
                           "║                Grid Size selected               ║\n" +
                           "║      Height =" + height + space + "Width =" + width + "         ║\n" +
                           "╚═════════════════════════════════════════════════╝\n");
        System.out.print("Continue with this ? (y/n) ");
        if (sc.next().toLowerCase().equals("n")) setSize(true);
        else {
            this.grid = new Grid(width, height);
        }
    }

    @Test
    public void init_test() {
        this.sc = new Scanner(System.in);
        this.ioHandler = new GameIOHandler();
        //setSize();
    }

    @Test
    private void loop_test() {

    }

    @Test
    public void startSave_test() {
        System.out.print("save@pc $> filedir -d ");
        String path = this.sc.nextLine();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss");
        LocalDateTime now = LocalDateTime.now();
        File file = new File(path + "battleship_" + dtf.format(now) + ".save");
        try {
            this.ioHandler.saveGame(file, this.grid);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void startLoad_test() {
        System.out.print("save@pc $> file -f ");
        String path = this.sc.nextLine();
        File file = new File(path);
        try {
            this.grid = this.ioHandler.loadGame(file);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void setGrid_test(Grid grid) {
        this.grid = grid;
    }

    @Test
    public Grid getGrid_test() {
        return grid;
    }
}
