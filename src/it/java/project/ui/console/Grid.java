package it.java.project.ui.console;

import it.java.project.entity.PlayerEntity;

import java.awt.*;
import java.io.Serializable;

/**
 * @author Miu
 */
public class Grid implements Serializable {

    public enum StateAttack {
        HIT, MISSED, NONE
    }

    /**
     *
     */
    private Dimension dimension;

    /**
     *
     */
    private PlayerEntity playerEntity1;
    private PlayerEntity playerEntity2;

    private int playerID = 0;

    public Grid(int width, int height) {
        this.dimension = new Dimension(width, height);
        this.playerEntity1 = null;
        this.playerEntity2 = null;
    }

    public void setPlayerEntity1(PlayerEntity playerEntity1) {
        this.playerEntity1 = playerEntity1;
    }

    public void setPlayerEntity2(PlayerEntity playerEntity2) {
        this.playerEntity2 = playerEntity2;
    }

    public boolean draw(final int playerID) {
        if (playerID == 0) {
            this.drawGrid(this.playerEntity1);
        } else {
            this.drawGrid(this.playerEntity2);
        }
        return true;
    }

    private void drawGrid(PlayerEntity playerEntity) {
        //line 1
        String line = Charset.TOP_LEFT_CORNER.getValue() + Charset.HORIZONTAL_LINE.getValue() + "Defense Grid";

        for (int i = line.length(); i < dimension.width + 3; i++)
            line += Charset.HORIZONTAL_LINE.getValue();

        line += Charset.T_LINE.getValue() + Charset.HORIZONTAL_LINE.getValue() + "Attack Grid";

        for (int i = line.length(); i < 2 * dimension.width + 6; i++)
            line += Charset.HORIZONTAL_LINE.getValue();

        line += Charset.TOP_RIGHT_CORNER.getValue() + '\n';

        //line 2 and other

        for (int i = 0; i < dimension.height; i++) {
            line += Charset.VERTICAL_LINE.getValue() + ' ';
            for (Charset v : playerEntity.getGridDefense()[i]) {
                line += v.getValue();
            }
            line += ' ' + Charset.VERTICAL_LINE.getValue() + ' ';
            for (StateAttack v : playerEntity.getGridAttack()[i]) {
                switch (v) {
                    case NONE:
                        line += Charset.INTERPUNCT.getValue();
                        break;
                    case HIT:
                        line += Charset.BLACK_SQUARE.getValue();
                        break;
                    case MISSED:
                        line += Charset.MULTILICATION_SIGN_CYAN.getValue();
                }
            }
            line += ' ' + Charset.VERTICAL_LINE.getValue()  + '\n';
        }

        //end
        line += Charset.BOTTOM_LEFT_CORNER.getValue();
        for (int i =  0; i < dimension.width + 2; i++) line += Charset.HORIZONTAL_LINE.getValue();
        line += Charset.T_LINE_REVERSE.getValue();
        for (int i =  0; i < dimension.width + 2; i++) line += Charset.HORIZONTAL_LINE.getValue();
        line += Charset.BOTTOM_RIGHT_CORNER.getValue()  + '\n';

        System.out.println(line);

    }

    public void playerAttack(int playerID, Point p) {
        if (playerID == 0) this.playerEntity1.attack(this.playerEntity2, p);
        else this.playerEntity2.attack(this.playerEntity1, p);
    }

    public int getHeight() {
        return this.dimension.height;
    }

    public int getWidth() {
        return this.dimension.width;
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setPlayerID(int playerID) {
        this.playerID = playerID;
    }

    public int getPlayerID() {
        return playerID;
    }
}
