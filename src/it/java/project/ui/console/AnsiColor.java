package it.java.project.ui.console;

/**
 * @author Miu
 */
public enum AnsiColor {

    ANSI_RESET("\u001B[0m"),
    ANSI_BLACK("\u001B[30m"),
    ANSI_RED("\u001B[31m"),
    ANSI_GREEN("\u001B[32m"),
    ANSI_YELLOW("\u001B[33m"),
    ANSI_BLUE("\u001B[34m"),
    ANSI_PURPLE("\u001B[35m"),
    ANSI_CYAN("\u001B[36m"),
    ANSI_WHITE("\u001B[37m"),
    HIGH_INTENSITY("\u001B[1m"),
    LOW_INTESITY("\u001B[2m");

    private String v;

    AnsiColor(String v) {
        this.v = v;
    }

    public String getValue() {
        return this.v;
    }
}
