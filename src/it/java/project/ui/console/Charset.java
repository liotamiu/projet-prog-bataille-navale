package it.java.project.ui.console;

/**
 * @author Miu and Mesabloo
 */
public enum Charset {

    INTERPUNCT(AnsiColor.ANSI_BLUE.getValue() + "·" + AnsiColor.ANSI_RESET.getValue()),
    MULTILICATION_SIGN_RED(AnsiColor.ANSI_RED.getValue() + AnsiColor.HIGH_INTENSITY.getValue() + "×" + AnsiColor.ANSI_RESET.getValue()),
    MULTILICATION_SIGN_CYAN(AnsiColor.ANSI_CYAN.getValue() + AnsiColor.HIGH_INTENSITY.getValue() + "×" + AnsiColor.ANSI_RESET.getValue()),
    BLOCK(AnsiColor.ANSI_GREEN.getValue() + "█" + AnsiColor.ANSI_RESET.getValue()),
    BLACK_SQUARE(AnsiColor.ANSI_RED.getValue() + "■" + AnsiColor.ANSI_RESET.getValue()),
    TOP_LEFT_CORNER("┌"),
    TOP_RIGHT_CORNER("┐"),
    BOTTOM_LEFT_CORNER("└"),
    BOTTOM_RIGHT_CORNER("┘"),
    HORIZONTAL_LINE("─"),
    VERTICAL_LINE("│"),
    T_LINE("┬"),
    T_LINE_REVERSE("┴");

    private final String v;

    Charset(String v) {
        this.v = v;
    }
    public String getValue() {
        return v;
    }
}