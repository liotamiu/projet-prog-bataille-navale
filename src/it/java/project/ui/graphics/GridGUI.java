package it.java.project.ui.graphics;

import javax.swing.*;
import java.awt.*;

public class GridGUI extends JPanel {

    public GridGUI() {
        super();

    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.drawRect(0, 0, this.getPreferredSize().width, this.getPreferredSize().height);


    }
}
