package it.java.project.ui.graphics;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.text.NumberFormat;

public class GameGUI extends JFrame {
    public GameGUI() {
        this.setPreferredSize(new Dimension(1000, 750));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);

        this.init();

        this.pack();
    }

    public void run() {
        this.setVisible(true);
    }

    private void init() {
        int width, height;

        JFormattedTextField widthTF = new JFormattedTextField(NumberFormat.getIntegerInstance()),
                            heightTF = new JFormattedTextField(NumberFormat.getIntegerInstance());

        do {
            int res = JOptionPane.showConfirmDialog(this, new Object[]{
                    "Width: ", widthTF,
                    "Height: ", heightTF
            }, "Battleship", JOptionPane.OK_CANCEL_OPTION);

            if (res == JOptionPane.OK_OPTION) {
                String widthRes = widthTF.getText(),
                       heightRes = heightTF.getText();
                try {
                    width = Integer.parseInt(widthRes);
                    height = Integer.parseInt(heightRes);
                    break;
                } catch (NumberFormatException e) {}
            }
        } while (true);

        // add grids later

        Border gridMargin = BorderFactory.createEmptyBorder(125, 0, 125, 0);
        GridGUI grid1 = new GridGUI();
        grid1.setPreferredSize(new Dimension(this.getPreferredSize().width / 2, this.getPreferredSize().width / 2));
        grid1.setBorder(gridMargin);

        this.add(grid1);
    }
}
