package it.java.project;

import com.sun.istack.internal.NotNull;

import it.java.project.ui.console.AnsiColor;
import it.java.project.ui.console.GameMenu;
import it.java.project.ui.graphics.GameGUI;

/**
 * @author Mesabloo and Miu
 */
public class Main {

    public static void main(@NotNull String[] args) {
        // args parser
        Boolean isCli = null, errored = false;
        for (String s : args) {
            if (s.equalsIgnoreCase("--cli") || s.equalsIgnoreCase("-c"))
                isCli = true;
            else if (s.equalsIgnoreCase("--gui") || s.equalsIgnoreCase("-g"))
                isCli = false;
            else {
                errored = true;
                System.err.println(AnsiColor.ANSI_RED.getValue() + "Unknown command-line argument `" + s + "`." + AnsiColor.ANSI_RESET.getValue());
            }
        }

        if (errored)
            return;

        if (isCli == null || isCli) {
            new GameMenu().run();
        } else {
            new GameGUI().run();
        }
    }
}