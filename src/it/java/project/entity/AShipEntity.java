package it.java.project.entity;

import com.sun.istack.internal.NotNull;
import it.java.project.ui.console.Charset;
import it.java.project.ui.console.Grid;

import java.awt.*;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author Mesabloo
 *
 */
public abstract class AShipEntity extends AEntity implements Comparable<AShipEntity> {
    public enum Axis {
        HORIZONTAL, VERTICAL, UNKNOWN
    }

    private int x, y;
    private Axis orientation;

    public AShipEntity(int x, int y, Axis orientation) {
        this.x = x;
        this.y = y;
        this.orientation = orientation;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public boolean[] getLives() {
        return this.lives;
    }

    public Axis getOrientation() {
        return this.orientation;
    }

    public int getLength() {
        return this.lives.length;
    }

    public boolean collideWith(AShipEntity s) {
        int x1 = this.getX(),
            y1 = this.getY(),
            x2 = s.getX(),
            y2 = s.getY();

        // HORIZONTAL => x1 ~> x1 + getLength()
        //            => x2 ~> x2 + getLength()
        // VERTICAL => y1 ~> y1 + getLength()
        //          => y2 ~> y2 + getLength()

        for (int i = 0;i < this.getLength();++i) {
            for (int j = 0;j < s.getLength();++j) {
                if (x1 == x2 && y1 == y2)
                    return true;

                if (s.getOrientation() == Axis.HORIZONTAL)
                    x2 += 1;
                else
                    y2 += 1;
            }

            if (s.getOrientation() == Axis.HORIZONTAL)
                x2 += 1;
            else
                y2 += 1;
        }

        return x1 == x2 && y1 == y2;
    }

    public boolean hit(Point p) {
        int x1 = this.getX(),
            y1 = this.getY();

        int i = 0;
        for (;i < this.getLength();++i) {
            if (x1 == p.getX() && y1 == p.getY()) {
                this.lives[i] = true;
                return true;
            }

            if (this.getOrientation() == Axis.HORIZONTAL)
                x1 += 1;
            else
                y1 += 1;
        }

        if (x1 == p.getX() && y1 == p.getY()) {
            this.lives[i] = true;
            return true;
        } else
            return false;
    }

    public boolean isInGrid(Grid g) {
        return this.x >= 0
               && this.y >= 0
               && this.x < g.getWidth()
               && this.y < g.getHeight();
    }

    public Charset[] toChars() {
        Charset[] draw = new Charset[this.lives.length];

        for (int i = 0;i < this.lives.length;++i)
            draw[i] = this.lives[i] ? Charset.MULTILICATION_SIGN_RED : Charset.BLOCK;

        return draw;
    }

    @Override
    public String toString() {
        return Arrays.stream(this.toChars()).map(Charset::getValue).collect(Collectors.joining());
    }

    @Override
    public int compareTo(@NotNull AShipEntity other) {
        return this.equals(other) ? 0 : -1;
    }

    // abstracts
    @Override
    public abstract boolean equals(@NotNull Object other);

    public abstract String getName();
}
