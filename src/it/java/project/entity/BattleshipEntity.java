package it.java.project.entity;

import it.java.project.ui.console.Charset;
import it.java.project.ui.console.Grid;

/**
 * @author Mesabloo
 *
 */
public class BattleshipEntity extends AShipEntity {
    public BattleshipEntity(int x, int y, Axis orientation) {
        super(x, y, orientation);
        this.lives = new boolean[] {
            false, false, false, false
        };
    }

    @Override
    public boolean collideWith(AShipEntity s) {
        return super.collideWith(s);
    }

    @Override
    public boolean isInGrid(Grid g) {
        return this.isInGrid(g);
    }

    @Override
    public Charset[] toChars() {
        return super.toChars();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public int compareTo(AShipEntity other) {
        return super.compareTo(other);
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof BattleshipEntity))
            return false;

        BattleshipEntity battleshipEntity = (BattleshipEntity) other;

        return battleshipEntity.getX() == this.getX()
               && battleshipEntity.getY() == this.getY()
               && battleshipEntity.getOrientation() == this.getOrientation()
               && battleshipEntity.getLives() == this.getLives();
    }

    @Override
    public String getName() {
        return "Battleship";
    }
}
