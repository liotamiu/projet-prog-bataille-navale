package it.java.project.entity;

import it.java.project.ui.console.Charset;
import it.java.project.ui.console.Grid;

public class SubmarineEntity extends AShipEntity {
    public SubmarineEntity(int x, int y, Axis orientation) {
        super(x, y, orientation);
        this.lives = new boolean[] {
                false, false, false, false
        };
    }

    @Override
    public boolean collideWith(AShipEntity s) {
        return super.collideWith(s);
    }

    @Override
    public boolean isInGrid(Grid g) {
        return this.isInGrid(g);
    }

    @Override
    public Charset[] toChars() {
        return super.toChars();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public int compareTo(AShipEntity other) {
        return super.compareTo(other);
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof SubmarineEntity))
            return false;

        SubmarineEntity submarineEntity = (SubmarineEntity) other;

        return submarineEntity.getX() == this.getX()
                && submarineEntity.getY() == this.getY()
                && submarineEntity.getOrientation() == this.getOrientation()
                && submarineEntity.getLives() == this.getLives();
    }

    @Override
    public String getName() {
        return "Submarine";
    }
}
