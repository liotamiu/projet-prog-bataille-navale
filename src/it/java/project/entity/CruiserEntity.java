package it.java.project.entity;

import it.java.project.ui.console.Charset;
import it.java.project.ui.console.Grid;

public class CruiserEntity extends AShipEntity {
    public CruiserEntity(int x, int y, Axis orientation) {
        super(x, y, orientation);
        this.lives = new boolean[] {
            false, false, false
        };
    }

    @Override
    public Charset[] toChars() {
        return super.toChars();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public int compareTo(AShipEntity other) {
        return super.compareTo(other);
    }

    @Override
    public boolean collideWith(AShipEntity s) {
        return super.collideWith(s);
    }

    @Override
    public boolean isInGrid(Grid g) {
        return super.isInGrid(g);
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof CruiserEntity))
            return false;

        CruiserEntity cruiserEntity = (CruiserEntity) other;

        return cruiserEntity.getX() == this.getX()
               && cruiserEntity.getY() == this.getY()
               && cruiserEntity.getOrientation() == this.getOrientation()
               && cruiserEntity.getLives() == this.getLives();
    }

    @Override
    public String getName() {
        return "Cruiser";
    }
}
