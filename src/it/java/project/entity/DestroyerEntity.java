package it.java.project.entity;

import it.java.project.ui.console.Charset;
import it.java.project.ui.console.Grid;

public class DestroyerEntity extends AShipEntity {
    public DestroyerEntity(int x, int y, AShipEntity.Axis orientation) {
        super(x, y, orientation);
        this.lives = new boolean[] {
                false, false, false, false
        };
    }

    @Override
    public boolean collideWith(AShipEntity s) {
        return super.collideWith(s);
    }

    @Override
    public boolean isInGrid(Grid g) {
        return this.isInGrid(g);
    }

    @Override
    public Charset[] toChars() {
        return super.toChars();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public int compareTo(AShipEntity other) {
        return super.compareTo(other);
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof DestroyerEntity))
            return false;

        DestroyerEntity destroyerEntity = (DestroyerEntity) other;

        return destroyerEntity.getX() == this.getX()
                && destroyerEntity.getY() == this.getY()
                && destroyerEntity.getOrientation() == this.getOrientation()
                && destroyerEntity.getLives() == this.getLives();
    }

    @Override
    public String getName() {
        return "Destroyer";
    }
}
