package it.java.project.entity;

import it.java.project.exception.EntityException;
import it.java.project.ui.console.Charset;
import it.java.project.ui.console.Grid;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Miu and Mesabloo
 */
public class PlayerEntity extends AEntity {

    /**
     * list of all ship of the player
     */
    private ArrayList<AShipEntity> shipEntities;

    /**
     * the attack and defense grid
     */
    private Charset[][] gridDefense;
    private Grid.StateAttack[][] gridAttack;

    /**
     * the construtor of the player
     * @param shipEntities the list of ship
     * @param d the size of the grid
     */
    public PlayerEntity(ArrayList<AShipEntity> shipEntities, Dimension d) {
        this.lives = new boolean[shipEntities.size()];
        this.shipEntities = shipEntities;
        orderShip();

        for (int i = 0; i < this.lives.length; i++)
            this.lives[i] = false;

        this.gridDefense = new Charset[d.height][d.width];
        this.gridAttack = new Grid.StateAttack[d.height][d.width];

        for (int i = 0; i < d.height; i++) for (int j = 0; j < d.width; j++) {
                this.gridDefense[i][j] = Charset.INTERPUNCT;
                this.gridAttack[i][j] = Grid.StateAttack.NONE;
        }

        this.updateGridDefense();
    }

    /**
     * this fuction are use for attack the other player
     * @param entity the target player
     * @param p the target point
     * @throws EntityException if the entity are not a instance of the player stop the game dude to a fatal error
     */
    public void attack(AEntity entity, Point p) {
        if (!(entity instanceof PlayerEntity)) throw new EntityException("Fatal Error: AEntity passed in function is not a PlayerEntity!\nGame Aborted");
        PlayerEntity playerEntity = ((PlayerEntity) entity);
        if (playerEntity.receiveAttack(p)) {
            this.gridAttack[p.y][p.x] = Grid.StateAttack.HIT;
            playerEntity.orderShip();
            playerEntity.updateGridDefense();
        }
        else
            this.gridAttack[p.y][p.x] = Grid.StateAttack.MISSED;
    }

    /**
     * this fuction is use by the attack function for checke if the target are on a ship
     * @param p the target point
     * @return the statue of the attack, hit or not
     */
    private boolean receiveAttack(Point p) {
        for (AShipEntity shipEntity : this.shipEntities) if (shipEntity.hit(p)) return true;
        return false;
    }

    /**
     * this fuction are use for order the ship insted of the list
     */
    private void orderShip() {
        this.shipEntities.sort((o1, o2) -> o1.compareTo(o2));
    }

    /**
     * this fuction are use for update the defense grid with the attack of the other player
     */
    private void updateGridDefense() {
        this.shipEntities.forEach(val -> {
            final Charset[] tmpTab = val.toChars();
            final AShipEntity.Axis axis = val.getOrientation();
            final int x = val.getX(),
                    y = val.getY();
            if (axis == AShipEntity.Axis.HORIZONTAL) {
                for (int i = 0; i < val.getLength(); i++)
                    this.gridDefense[y][x + i] = tmpTab[i];
            } else {
                for (int i = 0; i < val.getLength(); i++) {
                    this.gridDefense[y + i][x] = tmpTab[i];
                }
            }
        });
    }

    /**
     * get the defense drid
     * @return gridDefense
     */
    public Charset[][] getGridDefense() {
        return gridDefense;
    }

    /**
     * get the attack grid
     * @return gridAttack
     */
    public Grid.StateAttack[][] getGridAttack() {
        return gridAttack;
    }

    /**
     *fuction erited from Object
     * @return a String
     */
    @Override
    public String toString() {
        return null;
    }

    /**
     * fuction from AEntity use sepcalie in Ship
     * @param other N/A
     * @return N/A
     */
    @Deprecated
    @Override
    public boolean equals(Object other) {
        return false;
    }
}
