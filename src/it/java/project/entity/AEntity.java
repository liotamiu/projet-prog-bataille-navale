package it.java.project.entity;

import com.sun.istack.internal.NotNull;

import java.io.Serializable;

/**
 * @author Mesabloo
 *
 */
public abstract class AEntity implements Serializable {
    protected boolean[] lives;

    @Override
    public abstract String toString();

    @Override
    public abstract boolean equals(@NotNull Object other);
}
