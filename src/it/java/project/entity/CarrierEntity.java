package it.java.project.entity;

import com.sun.istack.internal.NotNull;
import it.java.project.ui.console.Charset;
import it.java.project.ui.console.Grid;

/**
 * @author Mesabloo
 *
 */
public class CarrierEntity extends AShipEntity {
    public CarrierEntity(int x, int y, Axis orientation) {
        super(x, y, orientation);
        this.lives = new boolean[] {
            false, false, false, false, false
        };
    }

    @Override
    public boolean collideWith(AShipEntity s) {
        return super.collideWith(s);
    }

    @Override
    public boolean isInGrid(Grid g) {
        return super.isInGrid(g);
    }

    @Override
    public Charset[] toChars() {
        return super.toChars();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public int compareTo(AShipEntity other) {
        return super.compareTo(other);
    }

    @Override
    public boolean equals(@NotNull Object other) {
        if (!(other instanceof CarrierEntity))
            return false;

        CarrierEntity carrierEntity = (CarrierEntity) other;

        return carrierEntity.getX() == this.getX()
               && carrierEntity.getY() == this.getY()
               && carrierEntity.getOrientation() == this.getOrientation()
               && carrierEntity.getLives() == this.getLives();
    }

    @Override
    public String getName() {
        return "Carrier";
    }
}
