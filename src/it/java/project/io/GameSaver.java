package it.java.project.io;

import it.java.project.ui.console.Grid;

import java.io.*;

/**
 * @author Miu
 */
public class GameSaver extends ObjectOutputStream {

    private final ObjectOutputStream outputStream;

    public GameSaver(File file) throws IOException {
        super();
        this.outputStream = new ObjectOutputStream(new FileOutputStream(file));
    }

    public void save(Grid toSave) throws IOException {
        this.outputStream.writeObject(toSave);
    }

}
