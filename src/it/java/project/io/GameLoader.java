package it.java.project.io;

import it.java.project.ui.console.Grid;

import java.io.*;

/**
 * @author Miu
 */
public class GameLoader {

    private final ObjectInputStream inputStream;

    public GameLoader(File file) throws IOException {
        super();
        this.inputStream = new ObjectInputStream(new FileInputStream(file));
    }

    public Grid load() throws IOException, ClassNotFoundException {
        return (Grid)this.inputStream.readObject();
    }
}
