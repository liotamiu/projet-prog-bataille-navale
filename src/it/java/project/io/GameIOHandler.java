package it.java.project.io;

import it.java.project.ui.console.Grid;

import java.io.File;
import java.io.IOException;

/**
 * @author Miu
 */
public class GameIOHandler {

    public void saveGame(File file, Grid grid) throws IOException{
        GameSaver saver = new GameSaver(file);
        saver.save(grid);
    }

    public Grid loadGame(File file) throws IOException, ClassNotFoundException {
        GameLoader loader = new GameLoader(file);
        return loader.load();
    }
}
