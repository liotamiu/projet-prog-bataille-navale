package it.java.project.exception;

import java.io.IOException;

/**
 * @author Mesabloo
 *
 */
public class LoadException extends IOException {

    public LoadException() {
        super();
    }

    public LoadException(String msg) {
        super(msg);
    }

    public LoadException(String msg, Throwable t) {
        super(msg, t);
    }
}
