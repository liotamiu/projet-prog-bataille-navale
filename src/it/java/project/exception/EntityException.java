package it.java.project.exception;

/**
 * @author Miu
 */
public class EntityException extends RuntimeException {

    public EntityException() {
        super();
    }

    public EntityException(String msg) {
        super(msg);
    }

    public EntityException(String msg, Throwable t) {
        super(msg, t);
    }
}
