package it.java.project.exception;

import java.io.IOException;

/**
 * @author Miu
 *
 */
public class SaveException extends IOException {

    public SaveException() {
        super();
    }

    public SaveException(String msg) {
        super(msg);
    }

    public SaveException(String msg, Throwable t) {
        super(msg, t);
    }
}
