package it.java.project.test;

import it.java.project.entity.AShipEntity;
import it.java.project.entity.BattleshipEntity;
import it.java.project.entity.CruiserEntity;
import it.java.project.entity.PlayerEntity;
import it.java.project.ui.console.Grid;

import java.awt.*;
import java.util.ArrayList;

/**
 * @author Miu
 */
public class Test_Grid {

    public static void main(String[] a) {
        //Engine e = new Engine<>(Test_Grid.class);
        //e.run();
        test();
    }

    public static void test() {
        Grid grid = new Grid(14, 14);
        ArrayList<AShipEntity> shipEntities = new ArrayList<>();
        shipEntities.add(new CruiserEntity(1, 2, AShipEntity.Axis.VERTICAL));
        shipEntities.add(new CruiserEntity(10, 3, AShipEntity.Axis.HORIZONTAL));
        grid.setPlayerEntity1(new PlayerEntity(shipEntities, grid.getDimension()));
        grid.draw(0);
        ArrayList<AShipEntity> shipEntities1 = new ArrayList<>();
        shipEntities1.add(new BattleshipEntity(10, 10, AShipEntity.Axis.VERTICAL));
        grid.setPlayerEntity2(new PlayerEntity(shipEntities1, grid.getDimension()));
        grid.draw(1);
        grid.playerAttack(0, new Point(10, 10));
        grid.draw(0);
        grid.draw(1);
        grid.playerAttack(1, new Point(1, 2));
        grid.draw(0);
        grid.draw(1);
        grid.playerAttack(0, new Point(12, 12));
        grid.draw(0);
        grid.draw(1);
    }
}
