package it.java.project.test;

import it.java.project.entity.AShipEntity;
import it.java.project.entity.CruiserEntity;
import it.java.project.test.engine.Engine;
import it.java.project.test.engine.exception.FailedMethodException;

import java.awt.*;

/**
 * @author Mesabloo
 */
public class Test_AShip {
    public static void main(String[] args) {
        Engine e = new Engine<>(Test_AShip.class);
        e.run();
    }

    public static void testShipHit() throws FailedMethodException {
        AShipEntity ship = new CruiserEntity(5, 5, AShipEntity.Axis.HORIZONTAL);
        Point p = new Point(6, 5);

        if (!ship.hit(p))
            throw new FailedMethodException("Expected ship to get hit.");
    }

    public static void testShipCollide() throws FailedMethodException {
        AShipEntity ship1 = new CruiserEntity(5, 5, AShipEntity.Axis.HORIZONTAL),
                    ship2 = new CruiserEntity(5, 4, AShipEntity.Axis.VERTICAL);

        if (!ship1.collideWith(ship2))
            throw new FailedMethodException("Expected ships to collide.");
    }

    /* ... */
}
