package it.java.project.test.engine.exception;

/**
 * @author Mesabloo
 */
public class FailedMethodException extends Exception {
    public FailedMethodException() {
        super();
    }

    public FailedMethodException(String msg) {
        super(msg);
    }

    public FailedMethodException(String msg, Throwable t) {
        super(msg, t);
    }
}
