package it.java.project.test.engine;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Mesabloo
 * @param <T>
 */
public class Engine<T> {
    private Class<T> c;

    public Engine(Class<T> c) {
        this.c = c;
    }

    public void run() {
        Method[] methods = this.c.getMethods();
        Map<String, String> results = new LinkedHashMap<>();

        Arrays.stream(methods)
            .filter(m -> !m.getName().equalsIgnoreCase("main"))
            .filter(m -> Modifier.isStatic(m.getModifiers()))
            .forEach(m -> {
            try {
                Object[] args = new Object[m.getParameterCount()];
                m.invoke(null, args);
                results.put(m.getName(), "Succeeded.");
            } catch (IllegalAccessException e) {
                results.put(m.getName(), "Could not access method.");
            } catch (InvocationTargetException e1) {
                results.put(m.getName(), "Failed with " + e1.getCause().toString());
            }
        });

        results.forEach((k, v) -> System.out.println(k + " - " + v));
    }
}
