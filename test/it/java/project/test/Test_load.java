package it.java.project.test;

import it.java.project.ui.console.GameMenu;
import it.java.project.ui.console.Grid;

public class Test_load {

    public static void main(String[] args) {
        GameMenu gameMenu = new GameMenu();
        gameMenu.init_test();
        gameMenu.startLoad_test();
        gameMenu.getGrid_test().draw(0);
        gameMenu.getGrid_test().draw(1);
    }
}
